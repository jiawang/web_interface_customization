# WEB interface customization baseline analysis

## Notebooks
[01_QA.ipynb](https://gitlab.wikimedia.org/jiawang/web_interface_customization/-/blob/main/01_QA.ipynb)
- QA instrumentation

[02_baseline.ipynb](https://gitlab.wikimedia.org/jiawang/web_interface_customization/-/blob/main/02_baseline.ipynb)
- Baseline analysis for: previews, full width and media viewer
- Code for result summary of https://phabricator.wikimedia.org/T346979#9285473


[03_mobile_skin_QA_baseline.ipynb](https://gitlab.wikimedia.org/jiawang/web_interface_customization/-/blob/main/03_mobile_skin_QA_baseline.ipynb)
- Re-QA font size instrumentation on mobile Minerva skin
- Baseline analysis of font size rate on mobile Minerva skin
- Code for result summary of https://phabricator.wikimedia.org/T346979#9318186


[04_baseline_pin.ipynb](https://gitlab.wikimedia.org/jiawang/web_interface_customization/-/blob/main/04_baseline_pin.ipynb)
- Baseline analysis for menu pin rate and  overall non-default rate
- Decision: for pin rate only considering viewport sizes larger than 1000px,  due to the different default value on smaller screens. 
- Code for result summary of https://phabricator.wikimedia.org/T346979#9376514


[06_baseline_viewport.ipynb](https://gitlab.wikimedia.org/jiawang/web_interface_customization/-/blob/main/06_baseline_viewport.ipynb)
- The final version of baseline analysis
  - Full width enable rate: Include all viewport groups.
  - Preview disable rate: Only include viewport sizes larger than 1200px.
  - Pin rate: Only include viewport sizes larger than 1000px.
  - Media view disable rate: Include all viewport groups.
  - Overall non-default rate of the above features: Only include viewport sizes larger than 1200px.
- Code for result summary of https://phabricator.wikimedia.org/T346979#9504012

